/*
    Copyleft Eduard BUDULEA 2021.
    This file is part of OpenVexed.

    OpenVexed is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenVexed is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/
package ro.budulea.vexed

import android.app.Activity
import android.app.AlertDialog
import android.content.*
import android.os.Bundle
import android.R.layout.*
import android.view.View
import android.widget.*

import ro.budulea.vexed.LevelPack
import ro.budulea.vexed.R

const val levelBoard = "level board"
const val levelTitle = "level title"
const val levelSolution = "level solution"

public class Lobby : Activity() {

    lateinit var levelPacks : List<String>
    var currentLevelPack = -1
    var currentLevel = 0
    lateinit var lvlInfo : TextView

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        levelPacks = (assets.list("")?:arrayOf("nimic")).map{it.toString()}
            .filter{it.endsWith(".ini")}.map{it.removeSuffix(".ini")}
        setContentView(R.layout.lobby)
        val lobby = this
        (findViewById(R.id.levelpacks) as Spinner) .apply {
            setOnItemSelectedListener(ChangeLevelPack(lobby))
            setAdapter(ArrayAdapter(lobby, android.R.layout.simple_spinner_item, lobby.levelPacks)
                .apply { setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            })
            val packNm = lobby.getSharedPreferences("lobby_info", Context.MODE_PRIVATE)
                .getString("currentLevelPack", "")
            setSelection(maxOf(0, lobby.levelPacks.indexOfFirst{it == packNm}))
        }
        lvlInfo = findViewById(R.id.level_info) as TextView
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) return
        data?.getStringExtra("WON")?.toIntOrNull()?.let {
            val prefs = getSharedPreferences("wins_info", Context.MODE_PRIVATE)
            val movesNR = prefs.getInt("${levelPacks[currentLevelPack]}/$currentLevel", 0)
            if (movesNR > it || movesNR == 0) prefs.edit()
                .putInt("${levelPacks[currentLevelPack]}/$currentLevel", it)
                .apply()
            updateLvlInfo()

            val levels = (findViewById(R.id.levels) as Spinner)
            val button = findViewById(R.id.play_level)
            if (currentLevel + 1 < levels.getCount()) {
                AlertDialog.Builder(this).apply {
                    setTitle("Do you want to go yo next level?")
                    setPositiveButton("Ok", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog : DialogInterface, whichButton : Int) {
                            levels.setSelection(++currentLevel)
                            button.performClick()
                        }
                    })
                    setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog : DialogInterface, whichButton : Int) { }
                    })
                    show()
                }
            }
        }
    }
    
    fun updateLvlInfo() {
        if (currentLevelPack < 0) lvlInfo.visibility = View.INVISIBLE
        else {
            lvlInfo.visibility = View.VISIBLE
            val prefs = getSharedPreferences("wins_info", Context.MODE_PRIVATE)
            val movesNR = prefs.getInt("${levelPacks[currentLevelPack]}/$currentLevel", 0)
            if (movesNR > 0)
            lvlInfo.text = getResources()
                .getQuantityString(R.plurals.level_info_win, movesNR, movesNR)
            else lvlInfo.setText(R.string.level_info_notwin)
        }
    }

    class ChangeLevelPack (val myParent : Lobby) : AdapterView.OnItemSelectedListener {
        var firstTry = true

        override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
            myParent.currentLevelPack = pos
            val levelPack = myParent.assets.open(myParent.levelPacks[pos] + ".ini")
                .bufferedReader().useLines{LevelPack(myParent.levelPacks[pos], it)}
            val levels = Array<String> (levelPack.levels.count(), {
                "${it + 1} - ${levelPack.levels[it].title}"})
            (myParent.findViewById(R.id.levels) as Spinner).apply {
                setOnItemSelectedListener(ChangeLevel(levelPack, myParent))
                setAdapter(ArrayAdapter(myParent, android.R.layout.simple_spinner_item, levels)
                    .apply {setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)})
                if (firstTry) {
                    firstTry = false
                    this.setSelection(myParent.getSharedPreferences("lobby_info", Context.MODE_PRIVATE)
                        .getInt("currentLevel", 0))
                }
            }
            myParent.findViewById(R.id.level_label).visibility = View.VISIBLE
            myParent.findViewById(R.id.levels).visibility = View.VISIBLE
        }

        override fun onNothingSelected(parent: AdapterView<*>) {
            myParent.currentLevelPack = -1
            myParent.findViewById(R.id.level_label).visibility = View.INVISIBLE
            myParent.findViewById(R.id.levels).visibility = View.INVISIBLE
        }
    }

    class ChangeLevel (val levelPack : LevelPack, val myParent : Lobby) : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
            Toast.makeText(myParent.getApplicationContext(),
                "${levelPack.levels[pos].title} selected", Toast.LENGTH_SHORT).show();
            myParent.currentLevel = pos
            myParent.getSharedPreferences("lobby_info", Context.MODE_PRIVATE).edit()
                .putString("currentLevelPack", myParent.levelPacks[myParent.currentLevelPack])
                .putInt("currentLevel", myParent.currentLevel)
                .apply()
            myParent.updateLvlInfo()
            (myParent.findViewById(R.id.play_level) as Button).apply {
                visibility = View.VISIBLE
                setOnClickListener(object : View.OnClickListener {
                    override fun onClick(view: View?) {
                        myParent.startActivityForResult(Intent(myParent, GameBoard::class.java).apply {
                            putExtra(levelBoard, levelPack.levels[myParent.currentLevel].board.toString())
                            putExtra(levelTitle, levelPack.levels[myParent.currentLevel].title)
                            //putExtra(levelSolution, levelPack.levels[pos].solution)
                        }, 2)
                    }
                })
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {
            Toast.makeText(myParent.getApplicationContext(), "Numic selectat", Toast.LENGTH_SHORT).show();
            myParent.findViewById(R.id.level_info).visibility = View.INVISIBLE
        }

    }

}


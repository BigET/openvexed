/*
    Copyleft Eduard BUDULEA 2021.
    This file is part of OpenVexed.

    OpenVexed is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenVexed is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/
package ro.budulea.vexed

import android.app.Activity
import android.content.*
import android.os.Bundle
import android.R.layout.*
import android.view.*
import android.widget.*
import android.view.animation.*
import android.media.MediaPlayer

import kotlin.math.abs

import ro.budulea.vexed.R

public class GameBoard : Activity(), View.OnTouchListener, Animation.AnimationListener {

    lateinit var board_grid : GridLayout
    lateinit var ghost_tile : ImageView
    var selected_tile : ImageView? = null
    lateinit var break_sound : MediaPlayer
    lateinit var fall_sound : MediaPlayer
    lateinit var rub_sound : MediaPlayer
    var minX = 0.0f
    var maxX = 0.0f
    var initialX = 0.0f
    var movesNr = 0
    var rub_startX = 0.0f

    var tileIdx = 0
    var tileIdxTarget = 0
    enum class AnimSteps {fall, destroy, idle}
    var animationState = AnimSteps.idle
    var destroingTilesCount = 0

    val history = ArrayList<String>()
    var historyIndex = 0
    lateinit var rewind_button : Button
    lateinit var fforward_button : Button

    class Index constructor (val cols : Int, val rows : Int, var idx : Int) {
        constructor(bg : GridLayout, idx : Int) : this(bg.columnCount, bg.rowCount, idx)
        constructor(old : Index, idx : Int) : this(old.cols, old.rows, idx)
        fun west() : Index ? {
            if (idx % cols == 0) return null
            --idx
            return this
        }
        fun east() : Index ? {
            if ((idx + 1) % cols == 0) return null
            ++idx
            return this
        }
        fun south() : Index ? {
            if (idx / cols + 1 >= rows) return null
            idx += cols
            return this
        }
        fun north() : Index ? {
            if (idx <= cols) return null
            idx -= cols
            return this
        }
        fun getWest() : Int? = if (idx % cols == 0) null else idx - 1
        fun getEast() : Int? = if ((idx + 1) % cols == 0) null else idx + 1
        fun getSouth() : Int? = if (idx / cols + 1 >= rows) null else idx + cols
        fun getNotrth() : Int? = if (idx <= cols) null else idx - cols
    }

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.board)
        (findViewById(R.id.temp_text) as TextView)
            .text = getIntent().getStringExtra(levelTitle)
        board_grid = (findViewById(R.id.the_board) as GridLayout)
        ghost_tile = (findViewById(R.id.ghost_tile) as ImageView)
        history.add(getIntent().getStringExtra(levelBoard).filter{it != '\n'})
        break_sound = MediaPlayer.create(this, R.raw.spargere)
        fall_sound = MediaPlayer.create(this, R.raw.cadere)
        rub_sound = MediaPlayer.create(this, R.raw.frecare)
        val one = this
        findViewById(R.id.restart).setOnClickListener(object : View.OnClickListener {
            override fun onClick(view : View?) {
                one.initBoard()
            }
        })
        rewind_button = findViewById(R.id.rewind) as Button
        rewind_button.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view : View?) {
                if (0 < one.historyIndex) {
                    one.populateBoard(one.history.get(--one.historyIndex))
                    one.registerListeners()
                }
            }
        })
        fforward_button = findViewById(R.id.ffwd) as Button
        fforward_button.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view : View?) {
                if (one.historyIndex + 1 < one.history.size) {
                    one.populateBoard(one.history.get(++one.historyIndex))
                    one.registerListeners()
                }
            }
        })
        initBoard()
    }

    fun initBoard() {
        historyIndex = 0
        history.subList(1, history.size).clear()
        populateBoard(history.get(0))
        registerListeners()
    }

    fun populateBoard(board_string: String) {
        for (i in 0 .. board_grid.getChildCount() - 1) {
            val tileView = board_grid.getChildAt(i) as ImageView
            val tileCode = board_string.get(i)
            when (tileCode) {
                in 'a'..'h' -> {
                    tileView.setImageResource(code2res(tileCode))
                    tileView.visibility = View.VISIBLE
                }
                ' ' -> tileView.visibility = View.INVISIBLE
            }
            tileView.contentDescription = tileCode.toString()
        }
    }

    fun recordBoard() : String = Array(board_grid.getChildCount()) {
            board_grid.getChildAt(it).contentDescription
        }.joinToString("")

    fun code2res(code : Char) : Int = when (code) {
        'a' -> R.drawable.block1
        'b' -> R.drawable.block2
        'c' -> R.drawable.block3
        'd' -> R.drawable.block4
        'e' -> R.drawable.block5
        'f' -> R.drawable.block6
        'g' -> R.drawable.block7
        'h' -> R.drawable.block8
        else -> R.drawable.wall
    }

    fun registerListeners() {
        var tidx = Index(board_grid, 0)
        for (i in 0 .. board_grid.getChildCount() - 1) {
            tidx.idx = i
            val tileView = board_grid.getChildAt(i)
            when (tileView.contentDescription.get(0)) {
                in 'a'..'h' -> {
                    if (isBlank(tidx.getWest()) || isBlank(tidx.getEast()))
                        tileView.setOnTouchListener(this)
                }
                else -> tileView.setOnTouchListener(null)
            }
        }
        rewind_button.setEnabled(historyIndex > 0)
        fforward_button.setEnabled(historyIndex + 1 < history.size)
    }

    fun disableListeners() {
        for (i in 0 .. board_grid.getChildCount() - 1)
            board_grid.getChildAt(i).setOnTouchListener(null)
    }

    fun cancelEvent() {
        selected_tile?.let {
            it.visibility = View.VISIBLE
            selected_tile = null
            ghost_tile.visibility = View.INVISIBLE
        }
    }

    fun isBlank(idx : Int?) : Boolean = isTile(idx, ' ')

    fun getTile(idx : Int?) : Char?
        = idx?.let {
            if (idx < 0 || idx >= board_grid.getChildCount()) null
            else board_grid.getChildAt(it).contentDescription.get(0)
        }

    fun isTile(idx : Int?, tileCode : Char) : Boolean
        = getTile(idx)?.equals(tileCode)?:false

    fun setAllowMovement(idx : Int) {
        val tilew = board_grid.width / board_grid.columnCount
        val tileCode = getTile(idx)!!
        var tidx = Index(board_grid, idx)
        while (isBlank(tidx.getWest()) && !isBlank(tidx.west()?.getSouth())
            && !isTile(tidx.getSouth(), tileCode)) ;
        minX = (tidx.idx % board_grid.columnCount * tilew).toFloat()
        tidx.idx = idx
        while (isBlank(tidx.getEast()) && !isBlank(tidx.east()?.getSouth())
            && !isTile(tidx.getSouth(), tileCode)) ;
        maxX = (tidx.idx % board_grid.columnCount * tilew).toFloat()
    }

    fun getIndex(y : Int, x : Int) = y * board_grid.rowCount / board_grid.height * board_grid.columnCount + x * board_grid.columnCount / board_grid.width

    fun moveTile(oldIdx : Int, newIdx : Int) {
        if (oldIdx == newIdx) return
        val ov = board_grid.getChildAt(oldIdx) as ImageView
        val nv = board_grid.getChildAt(newIdx) as ImageView
        nv.contentDescription = ov.contentDescription
        nv.visibility = View.VISIBLE
        nv.setImageResource(code2res(nv.contentDescription.get(0)))
        ov.contentDescription = " "
        ov.visibility = View.INVISIBLE
        ov.setImageResource(code2res(ov.contentDescription.get(0)))
    }

    fun getX(idx : Int) = (idx % board_grid.columnCount * board_grid.getChildAt(0).width).toFloat()
    fun getY(idx : Int) = (idx / board_grid.columnCount * board_grid.getChildAt(0).height).toFloat()

    fun animateMoveTile () {
        board_grid.getChildAt(tileIdx).visibility = View.INVISIBLE
        ghost_tile.setTranslationY(0.0f)
        ghost_tile.setTranslationX(0.0f)
        ghost_tile.setImageResource(code2res(board_grid.getChildAt(tileIdx).contentDescription.get(0)))
        ghost_tile.visibility = View.VISIBLE
        ghost_tile.alpha = 1.0f
        rub_sound.stop()
        rub_sound.prepare()
        fall_sound.start()
        ghost_tile.startAnimation(TranslateAnimation(getX(tileIdx), getX(tileIdxTarget), getY(tileIdx), getY(tileIdxTarget)).let {
            it.setDuration((100 * (tileIdxTarget - tileIdx) / board_grid.columnCount).toLong())
            it.setAnimationListener(this)
            it
        })
    }

    override fun onAnimationEnd(animation : Animation) {
        moveTile(tileIdx, tileIdxTarget)
        ghost_tile.visibility = View.INVISIBLE
        fall_sound.stop()
        fall_sound.prepare()
        runNextStep()
    }

    override fun onAnimationStart(animation : Animation) {}
    override fun onAnimationRepeat(animation : Animation) {}

    class DestroyListener (val state : GameBoard, val v : View) : Animation.AnimationListener {
        override fun onAnimationEnd(animation : Animation) {
            v.visibility = View.INVISIBLE
            val nStateCount = --state.destroingTilesCount
            if (nStateCount <= 0) state.stabilizeBoard()
        }
        override fun onAnimationStart(animation : Animation) {}
        override fun onAnimationRepeat(animation : Animation) {}
    }

    fun destroyTile(idx : Int) {
        val v = board_grid.getChildAt(idx)
        ++destroingTilesCount
        v.contentDescription = " "
        v.setOnTouchListener(null)
        break_sound.start()
        v.startAnimation(AlphaAnimation(1.0f, 0.0f).let {
            it.setDuration(500)
            it.setAnimationListener(DestroyListener(this, v))
            it
        })
    }

    fun fallTiles() {
        --tileIdx
        if (tileIdx < 0) {
            animationState = AnimSteps.destroy
            tileIdx = 0
        }
        if (board_grid.getChildAt(tileIdx).contentDescription.get(0) in 'a'..'h') {
            var downIdx = Index(board_grid, tileIdx)
            while (isBlank(downIdx.south()?.idx));
            downIdx.north()
            if (tileIdx < downIdx.idx) {
                tileIdxTarget = downIdx.idx
                animateMoveTile()
                return
            }
        }
        runNextStep()
    }

    fun runNextStep() {
        when (animationState) {
            AnimSteps.fall -> fallTiles()
            AnimSteps.destroy -> destroyTiles()
            AnimSteps.idle -> afterBoardSetteled()
        }
    }

    fun destroyTiles() {
        var destroiedSomething = false
        for (i in 0 .. board_grid.getChildCount() - 1) {
            val myCellCode = getTile(i) ?: ' '
            if (myCellCode in 'a'..'h') {
                var foundMatch = false
                var tile = Index(board_grid, i)
                if (isTile(tile.getSouth(), myCellCode)) {
                    destroyTile(tile.getSouth()!!)
                    foundMatch = true
                }
                if (isTile(tile.getEast(), myCellCode)) {
                    tile.east()
                    destroyTile(tile.idx)
                    foundMatch = true
                    if (isTile(tile.getEast(), myCellCode))
                        destroyTile(tile.getEast()!!)
                    if (isTile(tile.getSouth(), myCellCode))
                        destroyTile(tile.getSouth()!!)
                }
                destroiedSomething = destroiedSomething || foundMatch
                if (foundMatch) destroyTile(i)
            }
        }
        if (destroiedSomething) animationState = AnimSteps.fall
        else afterBoardSetteled()
    }

    fun stabilizeBoard() {
        tileIdx = board_grid.getChildCount() - board_grid.columnCount
        animationState = AnimSteps.fall
        runNextStep()
    }

    fun afterBoardSetteled() {
        if (emptyBoard()) {
            setResult(Activity.RESULT_OK, Intent().apply {
                putExtra("WON", movesNr.toString())
            })
            finish()
        }
        else {
            history.subList(historyIndex + 1, history.size).clear()
            history.add(recordBoard())
            historyIndex = history.size - 1
            registerListeners()
        }
    }

    fun emptyBoard() : Boolean {
        for (i in 0.. board_grid.getChildCount() - 1)
            if ((getTile(i) ?: ' ') in 'a'..'h') return false
        return true
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        val actionTaken = event.actionMasked
        if (actionTaken == MotionEvent.ACTION_DOWN && selected_tile != null)
            cancelEvent()
        fun limitedX() = (v.left.toFloat() + event.getX() - initialX).coerceIn(minX, maxX)
        return when (actionTaken) {
            MotionEvent.ACTION_DOWN -> {
                selected_tile = v as ImageView
                v.visibility = View.INVISIBLE
                ghost_tile.setTranslationY(v.top.toFloat())
                ghost_tile.setTranslationX(v.left.toFloat())
                ghost_tile.setImageResource(code2res(v.contentDescription.get(0)))
                ghost_tile.visibility = View.VISIBLE
                ghost_tile.alpha = 0.7f
                setAllowMovement(getIndex(v.top, v.left))
                initialX = event.getX()
                rub_startX = initialX
                true
            }
            MotionEvent.ACTION_MOVE -> {
                val myX = limitedX()
                ghost_tile.setTranslationX(myX)
                if (board_grid.width / board_grid.columnCount / 2 <= abs(myX - rub_startX)) {
                    rub_startX = myX
                    rub_sound.start()
                }
                true
            }
            MotionEvent.ACTION_UP -> {
                cancelEvent()
                val srcIndex = getIndex (v.top, v.left)
                val trgIndex = getIndex(v.top,
                    limitedX().toInt() + (board_grid.width - 1) / board_grid.columnCount / 2)
                if (trgIndex != srcIndex) {
                    moveTile(srcIndex, trgIndex)
                    ++movesNr
                    disableListeners()
                    stabilizeBoard()
                }
                true
            }
            MotionEvent.ACTION_CANCEL -> {
                cancelEvent()
                true
            }
            else -> false
        }
    }
}

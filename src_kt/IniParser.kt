/*
    Copyleft Eduard BUDULEA 2021.
    This file is part of OpenVexed.

    OpenVexed is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenVexed is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/
package ro.budulea.vexed

class IniGroup(val name : String, _assigments : Map<String, String>) {
    val assigments = _assigments.toMap()
}

fun parseIni(lines : Sequence<String>) = sequence {
    var groupName = ""
    var values = HashMap<String, String>()
    lines.filter{ !it.isEmpty() && !it.startsWith(";") }
        .map{it.removeSuffix("\r")}
        .forEach {
            if (it.startsWith("[") && it.endsWith("]")) {
                yield(IniGroup(groupName, values))
                groupName = it.removeSurrounding("[", "]")
                values.clear()
            } else
                values[it.substringBefore('=')] =
                    if (0 > it.indexOf('=')) "" else it.substringAfter('=')
        }
}

enum class Block(val value : Char) {
    empty(' '), wall('X'), cerc('a'), fulger('b'), cruce('c'),
    carou('d'), taiat('e'), patrat('f'), antena('g'), disc('h');
    companion object {
        fun txt2Block(ch : Char) : Block {
            for (blk in values()) if (blk.value == ch) return blk
            return empty
        }
    }
}
enum class UserSlideDirection {left, right}

class UserTransition (c : Char, r : Char) {
    val column = getNrCR(c)
    val row = getNrCR(r)
    val slide = if (c.isUpperCase()) UserSlideDirection.left
        else UserSlideDirection.right
    private fun getNrCR (a : Char) = Character.getNumericValue(a) -
        Character.getNumericValue(if (a.isUpperCase()) '@' else '`')
}

class Board (defs : String) {
    val encLines = defs.split("/")
    val lines = encLines.map{
        it.replace("10", "XXXXXXXXXX").flatMap{
            if (it in '1'..'9') "X".repeat((it.toInt() - '0'.toInt())).asIterable()
            else ("" + it).asIterable()
        }.map{Block.txt2Block(it)}
    }
    fun render() {
        for (ln in lines) println(ln.map{it.value}.joinToString(""))
    }
    override fun toString() =
        lines.map{it.map{it.value}.joinToString("")}.joinToString("\n")
}


class LevelPack (val name : String, content : Sequence<String>) {
    private val groups = parseIni(content).toList()
    private val general = groups.first{ it.name == "General" }
    val author = general.assigments["Author"]
    val url = general.assigments["URL"]
    val description = general.assigments["Description"]
    class GameLevel(asgs : Map<String,String>) { 
        val title = asgs["title"]
        val board = Board(asgs["board"]!!)
        private val solLine = asgs["solution"]!!
        val solution = Array<UserTransition>(solLine.length / 2, {
            UserTransition(solLine.get(it * 2), solLine.get(it * 2 + 1))
        })
    }
    val levels = groups.filter{ it.name == "Level" }
            .map{GameLevel(it.assigments)}.toList()
}


# OpenVexed

Android port of the palm pilot game Vexed

## Building

Just run make

You should have android sdk and kotlin copiler installed

## Credits

The artwork is from the original vexed game on sourceforge:

https://sourceforge.net/projects/vexed/files/

The sournds are from https://www.zapsplat.com/

The code is home made.

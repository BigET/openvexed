ANDROIDSDK=/usr/bin/
ANDROID_DALVIK_EXCHANGE=/usr/lib/android-sdk/build-tools/debian/dx
PLATFORM=/usr/lib/android-sdk/platforms/android-23/android.jar
MINSDK=19
APP=ro/budulea/vexed
VERSION_CODE=$(shell grep versionCode AndroidManifest.xml | cut -d "\"" -f2)
VERSION_NAME=$(shell grep versionName AndroidManifest.xml | cut -d "\"" -f2)
KEY_PASS ?=android
KEY_STORAGE_PASS ?=$(KEY_PASS)


R_JAVA=bld_java/$(APP)/R.java
KT_main=class/META-INF/main.kotlin_module

all: vexed.aligned.apk

vexed.apk: vexed.aligned.apk keystore.jks
	$(ANDROIDSDK)apksigner sign --ks keystore.jks --ks-key-alias androidkey --ks-pass pass:'$(KEY_STORAGE_PASS)' --key-pass pass:'$(KEY_PASS)' --out $@ $<

keystore.jks:
	$(ANDROIDSDK)keytool -genkeypair -keystore $@ -alias androidkey -validity 10000 -keyalg RSA -keysize 2048

%.aligned.apk: %.unsigned.apk
	$(ANDROIDSDK)zipalign -f -p 4 $< $@

%.unsigned.apk: dex/classes.dex AndroidManifest.xml 
	$(ANDROIDSDK)aapt package --version-code $(VERSION_CODE) --version-name $(VERSION_NAME) -f -v -F $@ -I $(PLATFORM) -M AndroidManifest.xml -S res -A assets dex

dex/classes.dex: class/META-INF/main.kotlin_module
	mkdir -p dex
	$(ANDROID_DALVIK_EXCHANGE) --dex --verbose --min-sdk-version=$(MINSDK) --output=$@ class

class/%.class: bld_java/%.java
	javac -bootclasspath $(PLATFORM) -classpath src -source 1.7 -target 1.7 $^

$(KT_main): $(wildcard src_kt/*.kt) $(wildcard src_kt/**/*.kt) $(R_JAVA)
	kotlinc -d class -include-runtime $< -cp $(PLATFORM) src_kt bld_java

$(R_JAVA): AndroidManifest.xml res/**/*
	mkdir -p bld_java
	$(ANDROIDSDK)aapt package -f -m -J bld_java -S res -M AndroidManifest.xml -I $(PLATFORM)

clean:
	rm -vfr	bld_java \
		class \
		*.unsigned.apk \
		*.aligned.apk \
		dex

distclean: clean
	[ ! -d dex ] || rmdir dex
	rm -vf *.apk

squeaky-clean: distclean
	@echo 'Warning! This will remove your signing keys!'
	@echo 'You have 5 seconds to press CTRL-C'
	@sleep 5
	rm -vf *.jks
